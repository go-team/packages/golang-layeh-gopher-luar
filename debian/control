Source: golang-layeh-gopher-luar
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Jongmin Kim <jmkim@pukyong.ac.kr>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-yuin-gopher-lua-dev
Standards-Version: 4.2.1
Homepage: https://github.com/layeh/gopher-luar
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-layeh-gopher-luar
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-layeh-gopher-luar.git
XS-Go-Import-Path: layeh.com/gopher-luar
Testsuite: autopkgtest-pkg-go

Package: golang-layeh-gopher-luar-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-yuin-gopher-lua-dev
Multi-Arch: foreign
Description: Simplifies data passing between Go and GopherLua
 A gopher-luar simplifies data passing to and from GopherLua.
 .
 This package provides the library for converting and passing the data
 types between Go and GopherLua.
 .
 Go type values are converted to GopherLua type:
  * Boolean values are converted to lua.LBool.
  * Numeric values are converted to lua.LNumber.
  * String values are converted to lua.LString.
  * A nil value is converted to lua.LNil.
  * Functions are converted to *lua.LFunction.
  * Arrays, channels, maps, pointers, slices, and structs are all
    converted to *lua.LUserData.
 .
 GopherLua type values are converted to Go type:
  * lua.LBool values are converted to bool.
  * lua.LNumber values are converted to float64.
  * lua.LString values are converted to string.
  * lua.LNil can be converted to any channel, func, interface, map,
    pointer, slice, unsafe pointer, or uintptr value.
